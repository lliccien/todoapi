<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::all();
        return response($users, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');
        $role = $request->input('role');

        if ($userExist = User::where('email', $email)->first()) {

            $response = [
                'message' => 'User already exists',
                'status' => 'danger',
            ];

            return response()->json($response, 404);
        }

        $user = new User([
            'email' => $email,
            'password' => bcrypt($password),
            'role' => $role,
        ]);

        $credentials = [
            'email' => $email,
            'password' => $password,
            'role' => $role,
        ];

        if ($user->save()) {

            $response = [
                'message' => 'User created',
                'status' => 'success',
                'user' => $user,
            ];
            return response()->json($response, 201);
        }

        $response = [
            'message' => 'An error occurred',
            'status' => 'danger',
        ];

        return response()->json($response, 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user != NULL) {
            $user->delete();
            $user = [
                'message' => 'User deleted',
                'status' => 'success',
                'user' => $user
            ];
            return response()->json($user, 200);
        }

        $response = [
            'message' => 'Error during deleting !!',
            'status' => 'danger',

        ];
        return response()->json($response, 404);
    }

    public function signin(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');

        if ($user = User::where('email', $email)->first()) {

            $credentials = [
                'email' => $email,
                'password' => $password,
                'role' => $user->role
            ];

            $token = null;
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'message' => 'Email or Password are incorrect',
                        'status' => 'danger',
                    ], 404);
                }
            } catch (JWTAuthException $e) {
                return response()->json([
                    'message' => 'failed_to_create_token',
                    'status' => 'danger',
                ], 404);
            }

            $response = [
                'message' => 'User signin',
                'status' => 'success',
                'user' => $user,
                'token' => $token
            ];
            return response()->json($response, 201);

        }

        $response = [
            'message' => 'Email or Password are incorrect',
            'status' => 'danger',
        ];

        return response()->json($response, 404);


    }

}
