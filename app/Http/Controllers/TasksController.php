<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();

        //var_dump($tasks);

        if (count($tasks) == 0) {
            $tasks = [
                'message' => 'List Tasks empty!!'
            ];
        }
        return response($tasks, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task($request->all());

        if ($task->save()) {
            $task = [
                'message' => 'Task created',
                'status' => 'success',
                'task' => $task
            ];
            return response()->json($task, 201);
        }

        $response = [
            'message' => 'Error during creation !!',
            'status' => 'danger',

        ];
        return response()->json($response, 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);

        if ($task != NULL) {
            $task = [
                'message' => 'Task found',
                'status' => 'success',
                'task' => $task
            ];
            return response()->json($task, 200);
        }
        $response = [
            'message' => 'Task not exist !!',
            'status' => 'warning',

        ];
        return response()->json($response, 404);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $task = Task::find($id);

        if ($task != NULL) {

            $task->name = $request->input('name');
            $task->priority = $request->input('priority');
            $task->deadline = $request->input('deadline');
            $task->user_id = $request->input('user_id');
            $task->save();
            $task = [
                'message' => 'Task updated',
                'status' => 'success',
                'task' => $task
            ];
            return response()->json($task, 200);
        }

        $response = [
            'message' => 'Error during updating !!',
            'status' => 'danger',

        ];
        return response()->json($response, 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);

        if ($task != NULL) {
            $task->delete();
            $task = [
                'message' => 'Task deleted',
                'status' => 'success',
                'task' => $task
            ];
            return response()->json($task, 200);
        }

        $response = [
            'message' => 'Error during deleting !!',
            'status' => 'danger',

        ];
        return response()->json($response, 404);
    }
}
