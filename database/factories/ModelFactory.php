<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'),
    ];
});


$factory->define(App\Task::class, function (Faker $faker){
    $unixTimestap = '1461067200';
    return [
        'name' => $faker->sentence(2, true),
        'priority' => $faker->randomDigit(),
        'deadline' => $faker->dateTimeBetween($startDate = 'now', $endDate = '1 year', $timezone = null)->format('Y-m-d')
    ];
});

