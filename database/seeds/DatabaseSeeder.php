<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create([
            'email' => 'admin@admin.com',
            'role' => 'admin',
        ]);

        factory(App\User::class, 5)->create([
            'role' => 'user',
        ])->each(function (App\User $user) {

            factory(App\Task::class)
                ->times(5)
                ->create([
                    'user_id' => random_int(2, 6)
                ]);
        });
    }
}
