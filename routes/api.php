<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware(['jwt.auth'])->group(function (){

    Route::resource('tasks', 'TasksController', [
        'except' => ['create', 'edit']
    ]);

    Route::resource('users', 'AuthController', [
        'except' => ['create', 'edit', 'store', 'update']
    ]);

    Route::post('/user/register', [
        'uses' => 'AuthController@store'
    ]);

});

Route::post('/user/signin', [
    'uses' => 'AuthController@signin'
]);

